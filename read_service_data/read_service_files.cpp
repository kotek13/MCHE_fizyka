#include "read_service_files.h"


bool readInterdlugFile(string city, tm date, DataContainer &container)
{
	fstream file;
	string file_dir = getPath(city, date) + "\\interdlug";
	
	file.open(file_dir, fstream::in);

	if (!file.is_open())
	{
		cerr << "Could not open file: " << file_dir << endl;
		return false;
	}

	cerr << "Sucessfully opened file: " << file_dir << endl;

	string line;
	
	getline(file, line); //---

	getline(file, line); //:prognoza: DD.MM.YYYY
	if (checkEOF(file)) return false;

	getline(file, line); //:url: 
	if (checkEOF(file)) return false;

	for (int i = 0; i < 15; i++)
	{
		if (!readInteriadlugDay(file, date, container))
		{
			cerr << "Failed reading day " << i << endl;
			return false;
		}
	}
	return true;
}
bool readInteriadlugDay(fstream &file, tm date, DataContainer &container)
{
	string line;
	tm forecastDate;

	getline(file, line); //'12.03':
	if (checkEOF(file)) 
		return false;
	else
	{
		forecastDate = readDate(line, "'%D.%M':");
		forecastDate.tm_year = date.tm_year; 
		forecastDate.tm_hour = date.tm_hour;
		forecastDate.tm_min = date.tm_min;
		forecastDate.tm_sec = date.tm_sec;
	}

	getline(file, line); //  :temperature: 4°C
	if (checkEOF(file)) 
		return false;
	else
	{
		vector<int> Values = readIntValues(line);
		if (Values.size() < 1)
		{
			cerr << "Failed to read temperature: \""<<line<<"\""<<endl;
			return false;
		}
		weatherRecord<int> record;
		record.setForecastTime(forecastDate);
		record.setOffsetTime(date);
		record.duration = (24 * 60 * 60);
		record.data_source = Source::interia_dlugoterminowa;
		record.value = Values[0];
		container.Temperature.push_back(record);
	}

	getline(file, line); //  :description: Zachmurzenie duże z przelotnymi opadami 
	if (checkEOF(file)) 
		return false;
	else
	{
		weatherRecord<string> record;
		record.setForecastTime(forecastDate);
		record.setOffsetTime(date);
		record.duration = (24 * 60 * 60);
		record.data_source = Source::interia_dlugoterminowa;
		record.value = string(line.begin()+16,line.end());
		container.Description.push_back(record);
	}

	getline(file, line); //  :wind_direction: N
	if (checkEOF(file)) 
		return false;
	else
	{
		Direction windDirection = readWindDirection(line,19);
		weatherRecord<Direction> record;
		record.setForecastTime(forecastDate);
		record.setOffsetTime(date);
		record.duration = (24 * 60 * 60);
		record.data_source = Source::interia_dlugoterminowa;
		record.value = windDirection;
		container.WindDirection.push_back(record);
	}

	getline(file, line); //  :wind_speed: 12 km/h
	if (checkEOF(file)) 
		return false;
	else
	{
		vector<double> Values = readDoubleValues(line);
		if (Values.size() < 1)
		{
			cerr << "Failed to read wind speed: \"" << line << "\"" << endl;
			return false;
		}
		weatherRecord<double> record;
		record.setForecastTime(forecastDate);
		record.setOffsetTime(date);
		record.duration = (24 * 60 * 60);
		record.data_source = Source::interia_dlugoterminowa;
		record.value = Values[0] * 1000 / 3600;
		container.WindSpeed.push_back(record);
	}

	getline(file, line); //  :pressure: 1023
	if (checkEOF(file)) 
		return false;
	else
	{
		vector<int> Values = readIntValues(line);
		if (Values.size() < 1)
		{
			cerr << "Failed to read pressuer: \"" << line << "\"" << endl;
			return false;
		}
		weatherRecord<int> record;
		record.setForecastTime(forecastDate);
		record.setOffsetTime(date);
		record.duration = (24 * 60 * 60);
		record.data_source = Source::interia_dlugoterminowa;
		record.value = Values[0];
		container.Preassure.push_back(record);
	}

	return true;
}

