#pragma once

#include <iostream>
#include <fstream>

#include "string_functions.h"
#include "data_container.h"


bool readInterdlugFile(string city, tm date, DataContainer &container);
bool readInteriadlugDay(fstream &file, tm date, DataContainer &container);
