#pragma once

enum Direction
{
	none, N, NNE, NE,
	ENE, E, ESE,
	SE, SSE, S, SSW, SW,
	WSW, W, WNW,
	NW, NNW
};

enum DataType
{
	temperature = 1 << 0,
	windSpeed = 1 << 1,
	winDirection = 1 << 2,
	preassure = 1 << 3,
	humidity = 1 << 4,
	description = 1 << 5
};

enum Source
{
	undefined,
	interia_dlugoterminowa,
	interia_krotkoterminowa,
	twoja_pogoda_dlugoterminowa,
	twoja_pogoda_krotkoterminowa,
	pogodynka,
	station = 1 << 20
};