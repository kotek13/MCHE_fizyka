#pragma once

#include <vector>

#include "enums.h"
#include "weatherRecord.h"

using namespace std;


typedef vector < weatherRecord<int> >  WRIntVec;
typedef vector < weatherRecord<double> >  WRDoubleVec;
typedef vector < weatherRecord<string> >  WRStringVec;
typedef vector < weatherRecord<Direction> >  WRDirectionVec;

class DataContainer
{
public:
	WRIntVec Temperature;
	WRIntVec WindChill;
	WRIntVec Preassure;
	WRDirectionVec WindDirection;
	WRDoubleVec WindSpeed;
	WRStringVec Description;
	WRIntVec Humidity;
	WRDoubleVec Rain;
};
