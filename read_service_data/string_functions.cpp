#pragma once

#include "string_functions.h"

using namespace std;


string numStr(int x, int align)
{
	string ret = to_string(x);
	while (ret.size() < align)
		ret = "0" + ret;
	return ret;
}

string getPath(string city, tm date)
{
	string path(ABSPATH);
	path += "\\" + numStr(date.tm_year + 1900, 4) + "-" + numStr(date.tm_mon + 1) + "-" + numStr(date.tm_mday) + "_" + numStr(date.tm_hour) + "-" + numStr(date.tm_min) + "-" + numStr(date.tm_sec);
	path += "\\" + city;
	return path;
}
string getPath(string city, string date)
{
	return string(ABSPATH) + "\\" + date + "\\" + city;
}

tm readDate(string line, string format, int startpoz)
{
	tm date;
	vector<int> Values = readIntValues(string(line.begin() + startpoz, line.end()));
	int vec_poz=0;
	bool flag = false;
	for (int i = 0; i < format.size(); i++)
	{
		if (flag)
		{
			if (vec_poz >= Values.size())
			{
				cerr << "Not enough values found in: \"" << line << "\" to match selected format: \""<<format<<"\"" << endl;
				return date;
			}
			switch(format[i])
			{
			case 'Y':
				date.tm_year = (int)Values[vec_poz] - 1900;
				vec_poz++;
				break;
			case 'M':
				date.tm_mon = (int)Values[vec_poz] - 1;
				vec_poz++;
				break;
			case 'D':
				date.tm_mday = (int)Values[vec_poz];
				vec_poz++;
				break;
			case 'h':
				date.tm_hour = (int)Values[vec_poz];
				vec_poz++;
				break;
			case 'm':
				date.tm_min = (int)Values[vec_poz];
				vec_poz++;
				break;
			case 's':
				date.tm_sec = (int)Values[vec_poz];
				vec_poz++;
				break;
			default:
				break;
			}
			flag = false;
			continue;
		}

		if (format[i] == '%')
			flag = true;
	}

	return date;
}

int readIntValue(string line, int startpoz, int &poz)
{
	poz = startpoz;
	int value = 0, sign = 1;
	if (line[poz] == '-')
	{
		sign = -1;
		poz++;
	}
	while (poz < line.size() && line[poz] >= '0' && line[poz] <= '9')
	{
		value *= 10;
		value += line[poz] - '0';
		poz++;
	}
	return sign*value;
}
vector<int> readIntValues(string line)
{
	vector<int> Values;
	for (int i = 0; i < line.size(); i++)
	{
		if (line[i] == '-' || (line[i] >= '0' && line[i] <= '9'))
			Values.push_back(readIntValue(line, i, i));
	}
	return Values;
}

double readDoubleValue(string line, int startpoz,int &poz)
{
	poz = startpoz;
	int value = 0, sign = 1, value2=0,div=1;
	if (line[poz] == '-')
	{
		sign = -1;
		poz++;
	}
	while (poz < line.size() && line[poz] >= '0' && line[poz] <= '9')
	{
		value *= 10;
		value += line[poz] - '0';
		poz++;
	}
	if (poz < line.size() && (line[poz] == '.' || line[poz]==','))
	{
		poz++;
		while (poz < line.size() && line[poz] >= '0' && line[poz] <= '9')
		{
			value2 *= 10;
			value2 += line[poz] - '0';
			div *= 10;
			poz++;
		}
	}
	return (double)(sign*(value*div+value2))/(double)(div);
}
vector<double> readDoubleValues(string line)
{
	vector<double> Values;
	for (int i = 0; i < line.size(); i++)
	{
		if (line[i] == '-' || (line[i] >= '0' && line[i] <= '9'))
			Values.push_back(readDoubleValue(line, i, i));
	}
	return Values;
}

Direction readWindDirection(string line, int startpoz, int &poz)
{
	poz = startpoz;
	if (line[poz] == 'N')
	{
		poz++;
		if (poz >= line.size())
			return Direction::N;
		if (line[poz] == 'N')
		{
			poz++;
			if (line[poz] == 'E')
				return Direction::NNE;
			return Direction::NNW;
		}
		else if (line[poz] == 'E')
			return Direction::NE;
		else if (line[poz] == 'W')
			return Direction::NW;
		return Direction::N;
	}
	else if (line[poz] == 'E')
	{
		poz++;
		if (poz >= line.size())
			return Direction::E;
		if (line[poz] == 'N')
			return Direction::ENE;
		else if (line[poz] == 'S')
			return Direction::ESE;
		return Direction::E;
	}
	else if (line[poz] == 'S')
	{
		poz++;
		if (poz >= line.size())
			return Direction::S;
		if (line[poz] == 'S')
		{
			poz++;
			if (line[poz] == 'E')
				return Direction::SSE;
			return Direction::SSW;
		}
		else if (line[poz] == 'E')
			return Direction::SE;
		else if (line[poz] == 'W')
			return Direction::SW;
		return Direction::S;
	}
	else if (line[poz] == 'W')
	{
		poz++;
		if (poz >= line.size())
			return Direction::W;
		if (line[poz] == 'N')
			return Direction::NNE;
		else if (line[poz] == 'E')
			return Direction::NE;
		return Direction::W;
	}
	return Direction::none;
}

bool checkEOF(fstream &file)
{
	if (file.eof())
	{
		cerr << "End Of File reached" << endl;
		return true;
	}
	return false;
}
