#pragma once

#include <time.h>
#include "enums.h"

template<class DataType> class weatherRecord
{
public:
	time_t forecast_time;
	long long int offset_time,duration;
	DataType value, value_min, value_max;
	Source data_source;

	weatherRecord();
	void setForecastTime(time_t time);
	void setForecastTime(tm time);
	void setOffsetTime(time_t logingTime);
	void setOffsetTime(tm logingTime);
};


template<class DataType>
weatherRecord<DataType>::weatherRecord()
{
	this->forecast_time=NULL;
	this->offset_time = NULL;
	this->duration=NULL;
	this->data_source = Source::undefined;
	/*
	this->value = DataType(NULL);
	this->value_min = DataType(NULL);
	this->value_max = DataType(NULL);
	*/
}

template<class DataType>
void weatherRecord<DataType>::setForecastTime(time_t time)
{
	this->forecast_time = time;
}

template<class DataType>
void weatherRecord<DataType>::setForecastTime(tm time)
{
	this->forecast_time = mktime(&time);
}

template<class DataType>
void weatherRecord<DataType>::setOffsetTime(time_t logingTime)
{
	if (logingTime < this->forecast_time)
		this->offset_time = (long long int)difftime(this->forecast_time, logingTime);
	else
		this->offset_time = -(long long int)difftime(logingTime, this->forecast_time);
}

template<class DataType>
void weatherRecord<DataType>::setOffsetTime(tm logingTime)
{
	time_t ttime = mktime(&logingTime);
	if (ttime < this->forecast_time)
		this->offset_time = (long long int)difftime(this->forecast_time, ttime);
	else
		this->offset_time = -(long long int)difftime(ttime, this->forecast_time);
}
