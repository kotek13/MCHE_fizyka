#pragma once

#include <string>
#include <time.h>
#include <vector>
#include <iostream>
#include <fstream>

#include "enums.h"
#include "defines.h"


using namespace std;

string numStr(int x, int align=2);

string getPath(string city, tm date);
string getPath(string city, string date);

tm readDate(string line, string format,int startpoz=0); 
/*foramt: 
%Y - year
%M - month
%D - day of month

%h - hour
%m - minute
%s - seconds

%% - '%' character

*/

int readIntValue(string line, int startpoz, int &poz = *(new int(0)));
vector<int> readIntValues(string line);

double readDoubleValue(string line, int startpoz, int &poz = *(new int(0)));
vector<double> readDoubleValues(string line);

Direction readWindDirection(string line, int startpoz, int &poz = *(new int(0)));

bool checkEOF(fstream &file); 