﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace GetNet2
{
    class Program
    {
        static void Main(string[] args)
        {
            // args[0] directory 
            // args[1] link
            if (args.Length > 2)
            {
                string OutputDir = args[0];
                string DataLink = args[1];
                try
                {
                    WebRequest Request = WebRequest.Create(DataLink);
                    if (Request != null)
                    {
                        WebResponse Response = Request.GetResponse();
                        if (Response != null)
                        {
                            Stream ResponseStream = Response.GetResponseStream();
                            if (ResponseStream != null)
                            {
                                string ReceivedData = new StreamReader(ResponseStream).ReadToEnd();
                                StringReader sReader = new StringReader(ReceivedData);
                                if (sReader != null)
                                {
                                    JsonTextReader Reader = new JsonTextReader(sReader);
                                    if (Reader != null)
                                    {
                                        string[] Lines = new string[2];
                                        StreamWriter Out = new StreamWriter(OutputDir);

                                        while (Reader.Read())
                                        {
                                            if (Reader.TokenType == JsonToken.PropertyName && Reader.Value != null)
                                            {
                                                Lines[0] += Reader.Value + "; ";
                                                if (Reader.Read())
                                                {
                                                    if (Reader.TokenType == JsonToken.String && Reader.Value != null)
                                                        Lines[1] += Reader.Value + "; ";
                                                }
                                                else
                                                    break;
                                            }
                                            else if (Reader.TokenType == JsonToken.EndObject)
                                            {
                                                if (Lines[0].Length > 0 && Lines[1].Length > 0)
                                                    Out.WriteLine("{0}\n{1}\n", Lines[0], Lines[1]);
                                                Lines[0] = "";
                                                Lines[1] = "";
                                            }
                                        }
                                        Out.Flush(); 
                                        Out.Close();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return;
        }
    }
}
